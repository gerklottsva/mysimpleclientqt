#include "mainwindow.h"
#include <QApplication>
#include "mydblcb.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    Tango::DeviceProxy device("sys/tangotest/3");
    MyDblCb cb;
    QObject::connect(&cb, SIGNAL(dblUpdated(double)), &w, SLOT(updateLabel(double)));
    device.subscribe_event("double_scalar", Tango::EventType::CHANGE_EVENT, &cb);
    return a.exec();
}
