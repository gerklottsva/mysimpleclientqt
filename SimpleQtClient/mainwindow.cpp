#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(this, SIGNAL(dataUpdate(double)), this, SLOT(updateLabel(double)));

    chart = new QChart();
    series = new QLineSeries();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chartView = new QChartView(chart);

    auto *layout = new QVBoxLayout();
    layout->addWidget(chartView);
    ui->groupBox_2->setLayout(layout);

    /*
    tmr = new QTimer(this);
    tmr->setInterval(1000);
    connect(tmr, SIGNAL(timeout()), this, SLOT(timerTick()));
    tmr->start();
    */
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateLabel(double value)
{
    ui->label->setText(QString::number(value));

    series->append(series->count(), value);
    chart->removeSeries(series);
    chart->addSeries(series);
    chart->createDefaultAxes();
}

void MainWindow::timerTick()
{
    try {
        Tango::DeviceProxy device("sys/tangotest/3");

        qDebug() << "sys/tangotest/3 ping: " << device.ping();

        auto res = device.read_attribute("double_scalar");

        double value;

        res >> value;

        emit dataUpdate(value);
    }  catch (Tango::DevFailed &e) {
        qDebug() << QString::fromStdString(std::string(e.errors[0].desc));
    }
}

