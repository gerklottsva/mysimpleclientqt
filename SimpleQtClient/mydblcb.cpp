#include "mydblcb.h"

MyDblCb::MyDblCb(QObject *parent)
    : QObject{parent}
{

}

void MyDblCb::push_event(Tango::EventData *ed)
{
    double value;
    Tango::DeviceAttribute &data = *(ed->attr_value);
    data >> value;
    emit dblUpdated(value);
}
