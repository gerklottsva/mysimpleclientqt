#ifndef MYDBLCB_H
#define MYDBLCB_H

#include <QObject>
#include <tango.h>

class MyDblCb : public QObject, public Tango::CallBack
{
    Q_OBJECT
public:
    explicit MyDblCb(QObject *parent = nullptr);
    virtual void push_event(Tango::EventData *ed);
signals:
    void dblUpdated(double);
};

#endif // MYDBLCB_H
